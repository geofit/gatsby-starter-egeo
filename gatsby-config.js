var dotenv = require("dotenv")
var dotenvExpand = require("dotenv-expand")
var myEnv = dotenv.config()
dotenvExpand.expand(myEnv)

module.exports = {
  flags: {
    //PRESERVE_WEBPACK_CACHE: true,
    //PRESERVE_FILE_DOWNLOAD_CACHE: true,
    //FAST_DEV: true,
    //FAST_REFRESH: true,
    DEV_SSR: false,
  },

  siteMetadata: {
    title: `eGEO`,
    subtitle: `Hub géospatial collaboratif`,
    description: `Solution Open Source sur-mesure pour communiquer et valoriser les données disponibles sur votre territoire ou vos infrastructures`,
    author: `GEOFIT`,
    siteUrl: process.env.PROJECT_PROTOCOL + "://" + process.env.PROJECT_HOST,
    icon: "",
    theme: "light",
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-typescript`,
    `gatsby-plugin-image`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-sitemap`,
    `gatsby-plugin-postcss`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/favicon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: "gatsby-plugin-eslint",
      options: {
        extensions: ["js", "jsx", "ts", "tsx"],
        exclude: ["node_modules", "bower_components", ".cache", "public"],
        stages: ["develop", "build-javascript"],
        emitWarning: true,
        failOnError: false,
      },
    },

    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
