module.exports = {
  future: {},
  content: [
    "./node_modules/@egeo/**/*.{js,jsx,ts,tsx}",
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        project: {
          //Default thème
          primary: "#2391C0",
          primaryHov: "#196A8D",
          primaryBg: "#f8fbf0",
          secondary: "#345230",
          secondaryHov: "#233B20",
        },
      },
      backgroundImage: {
        fond: "url('../images/fond.png')",
      },
    },
  },
  variants: {},
  plugins: [
    require("daisyui"),
    require("@tailwindcss/typography"),
    require("@tailwindcss/forms"),
  ],
}
