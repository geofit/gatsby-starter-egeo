<h1 align="center">
Gatsby eGEO Starter
</h1>

## 🚀 Quick start

1.  **Create a Gatsby site.**

    Use the Gatsby CLI to create a new site, specifying the starter.

    ```bash
    gatsby new portal https://gitlab.com/geofit/gatsby-starter-egeo.git
    ```

1.  **Start developing.**

    Navigate into your new site’s directory and start it up.

    ```bash
    cd portal/
    gatsby develop
    ```

1.  **Open the source code and start editing!**

    Your site is now running at http://localhost:8000

    Note: You'll also see a second link: http://localhost:8000/\_\_\_graphql. This is a tool you can use to experiment with querying your data. Learn more about using this tool in the Gatsby tutorial.

    Open the my-default-starter directory in your code editor of choice and edit src/pages/index.js. Save your changes and the browser will update in real time!

## 🎓 Learning Gatsby

Looking for more guidance? Full documentation for Gatsby lives [on the website](https://www.gatsbyjs.org/). Here are some places to start:

- **For most developers, we recommend starting with our [in-depth tutorial for creating a site with Gatsby](https://www.gatsbyjs.org/tutorial/).** It starts with zero assumptions about your level of ability and walks through every step of the process.

- **To dive straight into code samples, head [to our documentation](https://www.gatsbyjs.org/docs/).** In particular, check out the _Guides_, _API Reference_, and _Advanced Tutorials_ sections in the sidebar.

## License

MIT
