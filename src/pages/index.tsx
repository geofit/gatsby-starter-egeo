import React, { ReactElement } from "react"
import { graphql, PageProps, Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

function Index({ data }: PageProps): ReactElement {
  // @ts-ignore
  const site = data.site.siteMetadata
  return (
    <>
      <section>
        <div className="w-full bg-cover bg-center bg-fond h-160 z-0 pt-20">
          <div className="relative px-4 py-16 sm:px-6 sm:py-24 lg:py-20 lg:px-8">
            <h1 className="text-center text-4xl font-extrabold tracking-tight sm:text-5xl lg:text-6xl">
              <span className="block text-white">{site.title}</span>
              <span className="block text-project-primary">
                {site.subtitle}
              </span>
            </h1>
            <p className="mt-6 max-w-lg mx-auto text-center text-xl text-gray-200 sm:max-w-3xl">
              {site.description}
            </p>
            <div className="mt-10 max-w-sm mx-auto sm:max-w-none sm:flex sm:justify-center">
              <div className="space-y-4 sm:space-y-0 sm:mx-auto sm:inline-grid sm:grid-cols-1 sm:gap-5">
                <Link
                  to="/404"
                  className="flex items-center justify-center px-4 py-3 border border-transparent text-base font-extrabold rounded-md uppercase text-white bg-project-primary hover:bg-project-primaryHov sm:px-8"
                >
                  Découvrez nos cartes interactives
                </Link>
                <Link to="/404">
                  <button className="btn btn-primary">
                    Découvrez nos cartes interactives
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div className="w-full text-center text-2xl mt-10">
        Gatsby Starter pour le portail eGEO
      </div>
      <div className="w-full text-center text-sm mt-10">
        <StaticImage
          src="../images/logo-egeo.png"
          alt="Logo eGEO"
          height={40}
        ></StaticImage>
        <br></br>
        &copy;GEOFIT 2022
      </div>{" "}
    </>
  )
}

export default Index

export const query = graphql`
  query {
    site {
      siteMetadata {
        title
        subtitle
        description
      }
    }
  }
`
