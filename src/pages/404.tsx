import React, { ReactElement } from "react"
import { Link } from "gatsby"

interface Props {}

function Error404(_props: Props): ReactElement {
  //const [isMenuOpen, setMenuOpen] = useState(false)
  return (
    <div className="bg-white min-h-screen px-4 py-16 sm:px-6 sm:py-24 md:grid md:place-items-center lg:px-8">
      <div className="max-w-max mx-auto">
        <main className="sm:flex">
          <p className="text-4xl font-extrabold text-project-primary sm:text-5xl">
            404
          </p>
          <div className="sm:ml-6">
            <div className="sm:border-l sm:border-gray-200 sm:pl-6">
              <h1 className="text-4xl font-extrabold text-gray-700 tracking-tight sm:text-5xl">
                Page introuvable
              </h1>
              <p className="mt-1 text-base text-gray-500">
                Merci de vérifier l'URL dans la barre de recherche et réessayer.
              </p>
            </div>
            <div className="mt-10 flex space-x-3 sm:border-l sm:border-transparent sm:pl-6">
              <Link
                to="/"
                className="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-project-primary hover:bg-project-primaryHov focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-project-primaryHov"
              >
                Retour sur la page d'acceuil
              </Link>
              <Link
                to="/contact"
                className="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md text-project-secondary bg-gray-100 hover:bg-gray-300 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-300"
              >
                Contactez-nous
              </Link>
            </div>
          </div>
        </main>
      </div>
    </div>
  )
}

export default Error404
